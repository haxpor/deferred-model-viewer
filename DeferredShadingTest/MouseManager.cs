using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DeferredShadingTest
{
    public class MouseManager
    {
        float amountX;
        float amountY;
        int halfWidth;
        int halfHeight;
        float scrollDiffAmount;

        const float cameraRotSpeed = 0.3f;

        MouseState originalState;   //original state is the state as which the mouse is at the center
        MouseState preState;
        MouseState currState;

        bool isMouseRelativeEnabled;
        bool isAlwaysKeepMouseCenter;

        //test mouse clicked
        bool isLeftMouseClicked;
        bool isMiddleMouseClicked;
        bool isRightMouseClicked;

        // Get mouse position X
        public int X
        {
            get { return Mouse.GetState().X; }
        }

        // Get mouse position Y
        public int Y
        {
            get { return Mouse.GetState().Y; }
        }

        // Get the scroll diff amount
        public float ScrollDiffAmount
        {
            get { return scrollDiffAmount; }
        }

        // Relative mouse mode
        public bool MouseRelativeEnabled
        {
            get { return isMouseRelativeEnabled; }
            set { isMouseRelativeEnabled = value; }
        }

        // Keep mouse at the center as always or not
        public bool AlwaysKeepMouseAtCenter
        {
            get { return isAlwaysKeepMouseCenter; }
            set { isAlwaysKeepMouseCenter = value; }
        }

        // Camera rotation
        public float CameraRotX
        {
            get { return amountX; }
        }

        // Camera rotation
        public float CameraRotY
        {
            get { return amountY; }
        }

        // World rotation (inverse of camera)
        public float WorldRotX
        {
            get { return -amountX; }
        }

        // World rotation (inverse of camera)
        public float WorldRotY
        {
            get { return -amountY; }
        }

        // Prestate
        public MouseState PreState
        {
            get { return preState; }
        }

        // Current State
        public MouseState CurrentState
        {
            get { return currState; }
        }

        //Constructor
        public MouseManager(int width, int height)
        {
            Init(width, height);
        }

        // Set the initial state of mouse
        private void Init(int width, int height)
        {
            halfWidth = width / 2;
            halfHeight = height / 2;
            Mouse.SetPosition(halfWidth, halfHeight);
            originalState = Mouse.GetState();
            preState = Mouse.GetState();
        }

        private void PreUpdate()
        {
            currState = Mouse.GetState();

            //Clear everything first
            isLeftMouseClicked = false;
            isMiddleMouseClicked = false;
            isRightMouseClicked = false;
        }

        private void PostUpdate()
        {
            //update the previous state
            preState = currState;
        }

        /// <summary>
        /// Update the mouse.
        /// </summary>
        /// <remarks>This method will automatically calculates differential time used for others methods it will call later on.</remarks>
        /// <param name="gameTime">GameTime object.</param>
        public void Update(GameTime gameTime)
        {
            //calculate differential time
            float diffTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;

            PreUpdate();

            if (isMouseRelativeEnabled)
                UpdateMouseRelative(diffTime, currState);
            else if (isAlwaysKeepMouseCenter)
                PositionMouseAtCenter();
            UpdateScrollDiffAmount(diffTime, currState);
            //All mouse input will be processed here
            ProcessMouseInput(diffTime, currState);

            //User defined update method
            UserUpdate(diffTime);

            PostUpdate();
        }

        /// <summary>
        /// User defined update section.
        /// </summary>
        /// <remarks>Caller should implement this method in order to provide other functionality.</remarks>
        /// <param name="gameTime">GameTime object.</param>
        protected virtual void UserUpdate(float diffTime)
        {
            //do nothing here
        }

        // Update the scroll diff amount
        private void UpdateScrollDiffAmount(float diffTime, MouseState currState)
        {
            scrollDiffAmount = currState.ScrollWheelValue - preState.ScrollWheelValue;
        }

        // Update the relative mouse
        private void UpdateMouseRelative(float diffTime, MouseState currState)
        {
            if (originalState != currState)
            {
                int relX = currState.X - originalState.X;
                int relY = currState.Y - originalState.Y;

                //use accumulative minus to ease the furthur use
                amountX -= relX * diffTime * cameraRotSpeed;
                amountY -= relY * diffTime * cameraRotSpeed;

                Mouse.SetPosition(originalState.X, originalState.Y);
            }
        }

        // Isolate method to keep mouse at the center as always
        private void PositionMouseAtCenter()
        {
            Mouse.SetPosition(originalState.X, originalState.Y);
        }

        //Process mouse input
        private void ProcessMouseInput(float diffTime, MouseState currState)
        {
            //Left mouse click
            if (preState.LeftButton == ButtonState.Released && currState.LeftButton == ButtonState.Pressed)
                isLeftMouseClicked = true;
            //Middle mouse click
            if (preState.MiddleButton == ButtonState.Released && currState.MiddleButton == ButtonState.Pressed)
                isMiddleMouseClicked = true;
            //Right mouse click
            if (preState.RightButton == ButtonState.Released && currState.RightButton == ButtonState.Pressed)
                isRightMouseClicked = true;
        }

        /// <summary>
        /// Get whether the left mouse is clicked.
        /// </summary>
        public bool LeftMouseClicked
        {
            get { return isLeftMouseClicked; }
        }

        /// <summary>
        /// Get whether the middle mouse is clicked.
        /// </summary>
        public bool MiddleMouseClicked
        {
            get { return isMiddleMouseClicked; }
        }

        /// <summary>
        /// Get whether the right mouse is clicked.
        /// </summary>
        public bool RightMouseClicked
        {
            get { return isRightMouseClicked; }
        }

        // Clear all the amount value
        public void Clear()
        {
            amountX = 0.0f;
            amountY = 0.0f;
        }

        // Set the mouse position
        public void SetPosition(int x, int y)
        {
            Mouse.SetPosition(x, y);
        }

        /// <summary>
        /// Check whether the mouse's position is within the specified rectangle's coordinate or not.
        /// </summary>
        /// <param name="x">X position of object to be checked</param>
        /// <param name="y">Y position of object to be checked</param>
        /// <param name="width">Widht of object to be checked</param>
        /// <param name="height">Height of object to be checked</param>
        /// <returns>Return true if the mouse's position is within, otherwise false.</returns>
        public bool IsOnSight(int x, int y, int width, int height)
        {
            Rectangle rect = new Rectangle(x, y, width, height);
            return rect.Contains(X, Y);
        }

        /// <summary>
        /// Check whether the mouse's position is wihtin the specified rectangle or not.
        /// </summary>
        /// <param name="rect">Rectangle represent the object to be checked.</param>
        /// <returns>Return true if the mouse's position is within the rectangle, otherwise false.</returns>
        public bool IsOnSight(Rectangle rect)
        {
            return rect.Contains(X, Y);
        }
    }
}
