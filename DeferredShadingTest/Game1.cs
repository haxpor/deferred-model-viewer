using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using ContentShared;

namespace DeferredShadingTest
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        IntPtr drawSurface;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont debugFont;
        Effect basicMaterialEffect;

        //MouseManager
        MouseManager mouseManager;
        Vector3 camPos = new Vector3(0, 0, 0);

        //camera
        Matrix view, projection;

        //Key
        KeyboardState ks, lastKs;

        //light position
        Vector3 lightPos = new Vector3(60, 25, 60);

        FrameRateMeasurer frameRate;
        Vector2 frameRatePos = new Vector2(5, 12);

        //Deferred Shading
        DeferredShadingRenderer deferredRenderer;

        //Take the screenshot
        ResolveTexture2D screenshot;

        /// <summary>
        /// ** used only via Winform's call.
        /// </summary>
        public GraphicsDeviceManager Graphics
        {
            get { return graphics; }
        }

        /// <summary>
        /// Get the basic effect.
        /// </summary>
        public Effect BasicEffect
        {
            get { return basicMaterialEffect; }
        }

        /// <summary>
        /// Get or set the model global scale.
        /// </summary>
        public float GlobalModelScale
        {
            get { return GlobalSettings.GLOBAL_SCALE; }
            set 
            {
                GlobalSettings.GLOBAL_SCALE = value;
                RecalculateCameraPosition();
            }
        }

        public Game1(IntPtr drawSurface)
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            Content.RootDirectory = "Content";

            // if drawSurface is NOT (IntPtr)0 then it means pure runing of XNA game
            if (drawSurface != (IntPtr)0)
            {
                this.drawSurface = drawSurface;
                graphics.PreparingDeviceSettings += new EventHandler<PreparingDeviceSettingsEventArgs>(graphics_PreparingDeviceSettings);
                System.Windows.Forms.Control.FromHandle((this.Window.Handle)).VisibleChanged += new EventHandler(Game1_VisibleChanged);
            }
        }

        /// <summary>
        /// Set draw surface.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = drawSurface;
        }

        /// <summary>
        /// Keep invisibility of windows handle of XNA as it is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game1_VisibleChanged(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Control.FromHandle((this.Window.Handle)).Visible == true)
                System.Windows.Forms.Control.FromHandle((this.Window.Handle)).Visible = false;
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //by setting these shared variable, they will effect also the nest of content that use the effect file
            basicMaterialEffect = Content.Load<Effect>("Effects/MRT");
            basicMaterialEffect.Parameters["specularLightColor"].SetValue(Color.White.ToVector4());
            basicMaterialEffect.Parameters["ambientLightColor"].SetValue(new Vector4(0.11f, 0.11f, 0.11f, 1f));
            basicMaterialEffect.Parameters["light"].StructureMembers["Position"].SetValue(lightPos);
            basicMaterialEffect.Parameters["light"].StructureMembers["LookAt"].SetValue(new Vector3(0, 0, 0));
            basicMaterialEffect.Parameters["light"].StructureMembers["Color"].SetValue(Color.White.ToVector4() * 0.78f);
            debugFont = Content.Load<SpriteFont>("Fonts/Debug");

            //framerate
            frameRate = new FrameRateMeasurer(debugFont, spriteBatch);

            //mousemanager
            mouseManager = new MouseManager(graphics.GraphicsDevice.DisplayMode.Width, graphics.GraphicsDevice.DisplayMode.Height);
            mouseManager.AlwaysKeepMouseAtCenter = true;
            mouseManager.MouseRelativeEnabled = true;

            //deferred shading renderer
            deferredRenderer = new DeferredShadingRenderer(this, GraphicsDevice, Content, spriteBatch);

            //create the projection matrix on hand
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                graphics.GraphicsDevice.DisplayMode.AspectRatio, 0.1f, 10000f);
            basicMaterialEffect.Parameters["projection"].SetValue(projection);

            //create the screenshot
            int width = GraphicsDevice.PresentationParameters.BackBufferWidth;
            int height = GraphicsDevice.PresentationParameters.BackBufferHeight;
            screenshot = new ResolveTexture2D(GraphicsDevice, width, height, 1, GraphicsDevice.PresentationParameters.BackBufferFormat);

            // load a model right away if it's pure XNA Game running
            // Note: Using ship1 is for example only
            if (drawSurface == (IntPtr)0)
            {
                ModelXMLConfig modelConfig = new ModelXMLConfig();
                modelConfig.Name = "RightAwayModel";
                modelConfig.Scale = 1.0f;
                
                // model tag object
                UserTagModelLevel modelTag = new UserTagModelLevel();
                modelTag.Model = Content.Load<Model>("Meshes/ship/ship1");

                // mesh tag object (manually edit)
                UserTagMeshLevel meshTag = new UserTagMeshLevel();
                meshTag.Diffuse = Content.Load<Texture2D>("Meshes/ship/ship1_c");
                meshTag.Normal = Content.Load<Texture2D>("Meshes/ship/ship1_n");
                meshTag.Specular = Content.Load<Texture2D>("Meshes/ship/ship1_s");
                // mesh
                Mesh mesh = new Mesh();
                mesh.Index = 0;
                mesh.BoneIndex = -1;
                mesh.Tag = meshTag;

                modelConfig.Meshes.Add(mesh);
                modelConfig.Tag = modelTag;

                ResetCameraPosition(false);
                GlobalModelScale = modelConfig.Scale;

                SetModelConfig(modelConfig);
            }
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            float ksDiffTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 50f;
        
            //update camera
            mouseManager.Update(gameTime);

            //update the look ahead vector
            Matrix rotation = Matrix.Identity;
            
            // only calculate look ahead vector if current model is active
            if(deferredRenderer.GetScene().CurrentModel != null)
                rotation = Matrix.CreateRotationX(mouseManager.CameraRotY) * Matrix.CreateRotationY(mouseManager.CameraRotX);

            Vector3 lookAhead = new Vector3(0, 0, -1);
            lookAhead = Vector3.Transform(lookAhead, rotation);

            //update the up vector of camera
            Vector3 up = new Vector3(0, 1, 0);
            up = Vector3.Transform(up, rotation);

            //update movement
            Vector3 move = new Vector3(0, 0, 0);
            if (ks.IsKeyDown(Keys.W) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(0, 0, -1);
            }
            if (ks.IsKeyDown(Keys.S) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(0, 0, 1);
            }
            if (ks.IsKeyDown(Keys.A) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(-1, 0, 0);
            }
            if (ks.IsKeyDown(Keys.D) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(1, 0, 0);
            }
            if (ks.IsKeyDown(Keys.E) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(0, -1, 0);
            }
            if (ks.IsKeyDown(Keys.Q) && lastKs.IsKeyUp(Keys.LeftAlt))
            {
                move += new Vector3(0, 1, 0);
            }

            //update the camera's position
            Vector3 moveAmount = move * ksDiffTime;
            moveAmount = Vector3.Transform(moveAmount, rotation);

            if(deferredRenderer.GetScene().CurrentModel != null)
                camPos += moveAmount;

            lookAhead += camPos;

            //set the position of the camera
            basicMaterialEffect.Parameters["cameraPosition"].SetValue(camPos);

            view = Matrix.CreateLookAt(camPos, lookAhead, up);
            basicMaterialEffect.Parameters["view"].SetValue(view);

            //take a screenshot
            if (ks.IsKeyDown(Keys.Space) && lastKs.IsKeyUp(Keys.Space))
            {
                //copy the back buffer into the resolve texture
                GraphicsDevice.ResolveBackBuffer(screenshot);
                screenshot.Save("screenshot.jpg", ImageFileFormat.Jpg);
            }

            //update the deferred shading scene
            deferredRenderer.CameraPosition = camPos;
            deferredRenderer.View = view;
            deferredRenderer.Projection = projection;
            deferredRenderer.Update(gameTime);

            lastKs = ks;
            ks = Keyboard.GetState();

            base.Update(gameTime);
        }

        public void DrawDebugInfo(GameTime gameTime)
        {
            string line1 = "X: " + camPos.X + "\n";
            string line2 = "Y: " + camPos.Y + "\n";
            string line3 = "Z: " + camPos.Z + "\n";

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);

            //position
            spriteBatch.DrawString(debugFont, line1,
                new Vector2(5, 12 * 2), Color.White);
            spriteBatch.DrawString(debugFont, line2,
                new Vector2(5, 12 * 3), Color.White);
            spriteBatch.DrawString(debugFont, line3,
                new Vector2(5, 12 * 4), Color.White);

            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            deferredRenderer.Draw(gameTime);

            //draw framerate
            frameRate.CalculateFrameRateAndDrawIt(gameTime, frameRatePos);
            //draw debug info
            DrawDebugInfo(gameTime);

            base.Draw(gameTime);
        }

        public void SetModelConfig(ModelXMLConfig modelConfig)
        {
            // set to scene
            deferredRenderer.SetModelConfigForScene(modelConfig);
        }

        public void SetUpCamera(Vector3 modelCenter, float modelRadius)
        {
            camPos = modelCenter * GlobalSettings.GLOBAL_SCALE;
            // reposition the look at vector
            Vector3 lookAhead = new Vector3(0, 0, -1);
            lookAhead += camPos;
            view = Matrix.CreateLookAt(camPos, lookAhead, Vector3.Up);
            basicMaterialEffect.Parameters["view"].SetValue(view);

            // update to deferred shader
            deferredRenderer.CameraPosition = camPos;
            deferredRenderer.View = view;
        }

        public void RecalculateCameraPosition()
        {
            camPos *= GlobalSettings.GLOBAL_SCALE;

            // *due to winform starts first, then we have to check this
            if (basicMaterialEffect != null && deferredRenderer != null)
            {
                // reposition the look at vector
                Vector3 lookAhead = new Vector3(0, 0, -1);
                lookAhead += camPos;
                view = Matrix.CreateLookAt(camPos, lookAhead, Vector3.Up);
                basicMaterialEffect.Parameters["view"].SetValue(view);

                // update to deferred shader
                deferredRenderer.CameraPosition = camPos;
                deferredRenderer.View = view;
            }
        }

        public void ResetCameraPosition(bool isChainUpdated)
        {
            camPos = Vector3.Zero;

            if (isChainUpdated)
            {
                // reposition the look at vector
                Vector3 lookAhead = new Vector3(0, 0, -1);
                lookAhead += camPos;
                view = Matrix.CreateLookAt(camPos, lookAhead, Vector3.Up);
                basicMaterialEffect.Parameters["view"].SetValue(view);

                // update to deferred shader
                deferredRenderer.CameraPosition = camPos;
                deferredRenderer.View = view;
            }
        }

        public void SetWireFrameDrawMode(bool enabled)
        {
            deferredRenderer.SetWireFrameDrawMode(enabled);
        }
    }
}
