﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DeferredShadingTest
{
    public class FrameRateMeasurer
    {
        //framerate properties
        int frameCount = 0;
        float elapsedFrameTime = 0f;
        float frameInterval = 200f;
        float frameRate = 0f;

        SpriteFont font;
        SpriteBatch batch;

        public FrameRateMeasurer(SpriteFont font, SpriteBatch batch)
        {
            this.font = font;
            this.batch = batch;
        }

        public void CalculateFrameRateAndDrawIt(GameTime gameTime, Vector2 drawPos)
        {
            float elapsedTime = (float)gameTime.ElapsedRealTime.TotalMilliseconds;

            frameCount++;
            //update current time
            elapsedFrameTime += elapsedTime;

            if (elapsedFrameTime > frameInterval)
            {
                //calculate framerate (in unit of frame-per-second)
                frameRate = ((float)frameCount * 1000.0f) / elapsedFrameTime;

                frameCount = 0;
                elapsedFrameTime = 0;
            }

            //draw it
            batch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            batch.DrawString(font, "Framerate: " + (int)frameRate, drawPos, Color.White);
            batch.End();
        }
    }
}
