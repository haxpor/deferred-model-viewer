﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ContentShared;

namespace DeferredShadingTest
{
    public class DeferredShadingRenderer
    {
        /// <summary>
        /// GraphicsDevice from game class.
        /// </summary>
        GraphicsDevice graphicsDevice;

        /// <summary>
        /// ContentManager from game class.
        /// </summary>
        ContentManager content;

        /// <summary>
        /// SpriteBatch from game class.
        /// </summary>
        SpriteBatch spriteBatch;

        /// <summary>
        /// Game instance.
        /// </summary>
        Game1 game;

        /// <summary>
        /// Scene used to draw the content into.
        /// </summary>
        Scene scene;

        #region 3D Effect filess
        // Used to clear the buffer to appropriate value inside the buffer
        Effect clearGBufferEffect;
        Effect renderGBufferEffect;
        Effect directionalLightEffect;
        Effect pointLightEffect;
        Effect spotLightEffect;
        Effect combineLightEffect;

        #region Simple Bloomatic
        Effect bloomaticEffect;
        double alphaFrame;

        #endregion

        #endregion

        #region Bloom Post-processing Effect files

        Effect bloomExtractEffect;
        Effect bloomBlurEffect;
        Effect combineFinalEffect;

        RenderTarget2D bloomExtractTarget;
        RenderTarget2D bloomBlurTarget;

        #endregion

        #region Params need from the game class

        /// <summary>
        /// Camera position.
        /// </summary>
        Vector3 camPosition;
        public Vector3 CameraPosition
        {
            get { return camPosition; }
            set { camPosition = value; }
        }

        /// <summary>
        /// View matrix.
        /// </summary>
        Matrix view;
        public Matrix View
        {
            get { return view; }
            set { view = value; }
        }

        /// <summary>
        /// Projection matrix.
        /// </summary>
        Matrix projection;
        public Matrix Projection
        {
            get { return projection; }
            set { projection = value; }
        }

        #endregion

        #region Associate render targets

        /// <summary>
        /// All associate render-targets for GBuffer.
        /// </summary>
        RenderTarget2D colorRt;
        RenderTarget2D normalRt;
        RenderTarget2D depthRt;
        RenderTarget2D lightMapRt;
        RenderTarget2D combinedLightRt;

        #endregion

        /// <summary>
        /// Quad renderer used for clearing the buffer inside the GBuffer.
        /// </summary>
        QuadRenderer quadRenderer;

        /// <summary>
        /// Screen adjustment for the transformed screen coordinate.
        /// </summary>
        Vector2 screenAdjustment = Vector2.Zero;
        public Vector2 ScreenAdjustment
        {
            get { return screenAdjustment; }
            set { screenAdjustment = value; }
        }

        /// <summary>
        /// Sphere model used to draw the point light.
        /// </summary>
        Model sphereModel;

        /// <summary>
        /// Cone model used to draw the spotlight.
        /// </summary>
        Model coneModel;

        public DeferredShadingRenderer(Game1 game, GraphicsDevice graphicsDevice, ContentManager content, SpriteBatch spriteBatch)
        {
            this.graphicsDevice = graphicsDevice;
            this.content = content;
            this.spriteBatch = spriteBatch;
            this.game = game;

            //create the quad renderer
            quadRenderer = new QuadRenderer(this.graphicsDevice);

            //load the the associate effect files
            clearGBufferEffect = this.content.Load<Effect>("Effects/ClearGBuffer");
            renderGBufferEffect = this.content.Load<Effect>("Effects/RenderGBuffer");
            directionalLightEffect = this.content.Load<Effect>("Effects/DirectionalLight");
            pointLightEffect = this.content.Load<Effect>("Effects/PointLight");
            spotLightEffect = this.content.Load<Effect>("Effects/Spotlight");
            combineLightEffect = this.content.Load<Effect>("Effects/CombineLight");

            //load effect
            bloomExtractEffect = this.content.Load<Effect>("Effects/BloomExtract");
            bloomBlurEffect = this.content.Load<Effect>("Effects/BloomBlur");
            combineFinalEffect = this.content.Load<Effect>("Effects/CombineFinal");

            //load the unit sphere
            sphereModel = this.content.Load<Model>("Meshes/sphere");
            coneModel = this.content.Load<Model>("Meshes/cone");

            //initialize all of the render-targets
            int backBufferWidth = graphicsDevice.PresentationParameters.BackBufferWidth;
            int backBufferHeight = graphicsDevice.PresentationParameters.BackBufferHeight;

            //set the screen adjustment
            screenAdjustment.X = 0.5f / backBufferWidth;
            screenAdjustment.Y = 0.5f / backBufferHeight;

            colorRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            normalRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            depthRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Single);

            lightMapRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);
            combinedLightRt = new RenderTarget2D(graphicsDevice, backBufferWidth, backBufferHeight, 1, SurfaceFormat.Color);

            //create the bloom extract, and bloom blur target with the dimension half of the back buffer (avoid the fillrate)
            bloomExtractTarget = new RenderTarget2D(graphicsDevice, backBufferWidth / 2, backBufferHeight / 2, 1, SurfaceFormat.Color);
            bloomBlurTarget = new RenderTarget2D(graphicsDevice, backBufferWidth / 2, backBufferHeight / 2, 1, SurfaceFormat.Color);

            //initialize scene
            //initialize this way will give the control of effect files to deffered shading renderer class
            scene = new Scene(game, this.graphicsDevice, renderGBufferEffect, this.content);
        }

        /// <summary>
        /// Set the render-target to be ready for drawing on for GBuffer.
        /// </summary>
        private void SetGBuffer()
        {
            graphicsDevice.SetRenderTarget(0, colorRt);
            graphicsDevice.SetRenderTarget(1, normalRt);
            graphicsDevice.SetRenderTarget(2, depthRt);
        }

        /// <summary>
        /// Resolve all the render-targets in GBuffer.
        /// </summary>
        private void ResolveGBufer()
        {
            graphicsDevice.SetRenderTarget(0, null);
            graphicsDevice.SetRenderTarget(1, null);
            graphicsDevice.SetRenderTarget(2, null);
        }

        /// <summary>
        /// Clear the buffer of all the render-targets in GBuffer.
        /// </summary>
        private void ClearGBuffer()
        {
            clearGBufferEffect.Begin();
            clearGBufferEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            clearGBufferEffect.CurrentTechnique.Passes[0].End();
            clearGBufferEffect.End();
        }

        public void Update(GameTime gameTime)
        {
            scene.Update(gameTime);

            //update alpha frame
            alphaFrame += (gameTime.ElapsedGameTime.TotalSeconds * 2.0);
        }

        public void Draw(GameTime gameTime)
        {
            //set render targets
            SetGBuffer();
            //clear render targets
            ClearGBuffer();

            //draw scene
            scene.Draw(gameTime);

            //resolve all render targets
            ResolveGBufer();

            //draw all the texture on screen
            /*int halfWidth = graphicsDevice.Viewport.Width / 2;
            int halfHeight = graphicsDevice.Viewport.Height / 2;

            spriteBatch.Begin();
            spriteBatch.Draw(colorRt.GetTexture(), new Rectangle(0, 0, halfWidth, halfHeight), Color.White);
            spriteBatch.Draw(normalRt.GetTexture(), new Rectangle(halfWidth, 0, halfWidth, halfHeight), Color.White);
            spriteBatch.Draw(depthRt.GetTexture(), new Rectangle(0, halfHeight, halfWidth, halfHeight), Color.White);
            spriteBatch.End();*/

            //draw the directional light
            graphicsDevice.Clear(Color.Black);
            DrawLights(gameTime);
            DrawNormalScene(gameTime);
            //DrawPostProcessing(gameTime);
        }

        private void DrawLights(GameTime gameTime)
        {
            //#1 LIGHT MAP
            //render the light into light render target
            graphicsDevice.SetRenderTarget(0, lightMapRt);

            //clear all components to 0 (0,0,0,0)
            graphicsDevice.Clear(Color.TransparentBlack);
            graphicsDevice.RenderState.AlphaBlendEnable = true;
            //add those color
            graphicsDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
            graphicsDevice.RenderState.DestinationBlend = Blend.One;
            graphicsDevice.RenderState.SourceBlend = Blend.One;
;
            //use the same operation for alpha channel
            graphicsDevice.RenderState.SeparateAlphaBlendEnabled = false;

            //insert the lights here
            //draw some lights
            DrawDirectionalLight(new Vector3(0, -1, 0), Color.White);
            DrawDirectionalLight(new Vector3(0, 1, 0), Color.White);
            DrawDirectionalLight(new Vector3(-1, 0, 0), Color.White);
            DrawDirectionalLight(new Vector3(1, 0, 0), Color.White);
            //DrawDirectionalLight(new Vector3(1, 0, 0), Color.Coral);

            //DrawPointLight(new Vector3(10, 20, 0), Color.White, 10.0f, 25f);
            //DrawPointLight(new Vector3(-10, 20, 0), Color.SandyBrown, 10.0f, 30f);
            //DrawPointLight(new Vector3(30 * (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds), 20,
            //    30 * (float)Math.Cos(gameTime.TotalGameTime.TotalSeconds)), Color.Coral, 10.0f, 35f);

            //DrawSpotlight(new Vector3(0, 25, 0), Color.White, Vector3.Down, 1.0f, MathHelper.ToRadians(45), 25f, 2f);


            //restore the alphablending
            graphicsDevice.RenderState.AlphaBlendEnable = false;

            graphicsDevice.SetRenderTarget(0, null);

            //render into the combined light render target
            graphicsDevice.SetRenderTarget(0, combinedLightRt);

            //#2 Combine the light map with the scene map
            //set the light map
            graphicsDevice.Textures[1] = lightMapRt.GetTexture();

            //combine the final color with light, to produce the input for post-processing
            combineLightEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            combineLightEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(colorRt.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

            combineLightEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            combineLightEffect.End();

            graphicsDevice.SetRenderTarget(0, null);
        }

        private void DrawNormalScene(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);

            spriteBatch.Draw(combinedLightRt.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

            spriteBatch.End();   
        }

        private void DrawPostProcessing(GameTime gameTime)
        {
            //#3 Do the post-processing
            //-3.1: apply the bloom extract
            graphicsDevice.SetRenderTarget(0, bloomExtractTarget);

            bloomExtractEffect.Parameters["BloomThreshold"].SetValue(0.5f);

            bloomExtractEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            bloomExtractEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(combinedLightRt.GetTexture(), new Rectangle(0, 0, bloomExtractTarget.Width, bloomExtractTarget.Height), Color.White);

            bloomExtractEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            bloomExtractEffect.End();

            graphicsDevice.SetRenderTarget(0, null);

            //-3.2: apply the blur effect on the bright area, draw it into the bloom blur render target
            graphicsDevice.SetRenderTarget(0, bloomBlurTarget);

            bloomBlurEffect.Parameters["blurMag"].SetValue(0.008f);
            bloomBlurEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            bloomBlurEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(bloomExtractTarget.GetTexture(), new Rectangle(0, 0, bloomBlurTarget.Width, bloomBlurTarget.Height), Color.White);

            bloomBlurEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            bloomBlurEffect.End();

            graphicsDevice.SetRenderTarget(0, null);

            //combine the two
            //set the blur texture
            graphicsDevice.Textures[1] = bloomBlurTarget.GetTexture();

            combineFinalEffect.Parameters["sceneIntensity"].SetValue(1f);
            combineFinalEffect.Parameters["bloomIntensity"].SetValue(2f);
            combineFinalEffect.Parameters["sceneSaturation"].SetValue(1f);
            combineFinalEffect.Parameters["bloomSaturation"].SetValue(0f);

            combineFinalEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            combineFinalEffect.CurrentTechnique.Passes[0].Begin();

            spriteBatch.Draw(combinedLightRt.GetTexture(), new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth,
                graphicsDevice.PresentationParameters.BackBufferHeight), Color.White);

            combineFinalEffect.CurrentTechnique.Passes[0].End();
            spriteBatch.End();
            combineFinalEffect.End();
        }

        private void DrawDirectionalLight(Vector3 lightDirection, Color color)
        {
            //set all parameters
            directionalLightEffect.Parameters["lightDirection"].SetValue(lightDirection);
            directionalLightEffect.Parameters["lightColor"].SetValue(color.ToVector3());
            directionalLightEffect.Parameters["cameraPosition"].SetValue(camPosition);
            directionalLightEffect.Parameters["invertedViewProjection"].SetValue(Matrix.Invert(view * projection));
            directionalLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            directionalLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            directionalLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            directionalLightEffect.Parameters["adjust"].SetValue(ScreenAdjustment);

            directionalLightEffect.Begin();
            directionalLightEffect.CurrentTechnique.Passes[0].Begin();
            quadRenderer.Draw();
            directionalLightEffect.CurrentTechnique.Passes[0].End();
            directionalLightEffect.End();
        }

        private void DrawPointLight(Vector3 lightPosition,
                                    Color lightColor,
                                    float lightIntensity,
                                    float lightRadius)
        {
            //set all parameters
            pointLightEffect.Parameters["LightPosition"].SetValue(lightPosition);
            pointLightEffect.Parameters["LightColor"].SetValue(lightColor.ToVector3());
            pointLightEffect.Parameters["LightIntensity"].SetValue(lightIntensity);
            pointLightEffect.Parameters["LightRadius"].SetValue(lightRadius);
            pointLightEffect.Parameters["CameraPosition"].SetValue(camPosition);
            pointLightEffect.Parameters["InvertedViewProjection"].SetValue(Matrix.Invert(view * projection));
            pointLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            pointLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            pointLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());
            pointLightEffect.Parameters["adjust"].SetValue(ScreenAdjustment);

            //sphere is the unit in radius
            Matrix sphereWorld = Matrix.CreateScale(lightRadius) * Matrix.CreateTranslation(lightPosition);

            pointLightEffect.Parameters["world"].SetValue(sphereWorld);
            pointLightEffect.Parameters["view"].SetValue(view);
            pointLightEffect.Parameters["projection"].SetValue(projection);

            //calculate the length from camera to the light center
            float distance = Vector3.Distance(camPosition, lightPosition);
            //if we are inside the light volume then we draw the interior of the sphere
            if (distance < lightRadius)
                graphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            else
                graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            //also disable the depth buffer (we don't actually draw anything into the scene.)
            graphicsDevice.RenderState.DepthBufferEnable = false;

            //prepare to draw the sphere (assume only one meshpart)
            ModelMesh mesh = sphereModel.Meshes[0];
            ModelMeshPart part = mesh.MeshParts[0];

            pointLightEffect.Begin();
            pointLightEffect.CurrentTechnique.Passes[0].Begin();

            graphicsDevice.VertexDeclaration = part.VertexDeclaration;
            graphicsDevice.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
            graphicsDevice.Indices = mesh.IndexBuffer;
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, 
                part.PrimitiveCount);

            pointLightEffect.CurrentTechnique.Passes[0].End();
            pointLightEffect.End();

            //restore back the cullmode and depth buffer
            graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            graphicsDevice.RenderState.DepthBufferEnable = true;
        }

        //*still need to fix
        private void DrawSpotlight(Vector3 position, Color color, Vector3 direction, float lightIntensity,
                                   float coneAngle, float radius, float decayExponent)
        {
            spotLightEffect.Parameters["CameraPosition"].SetValue(camPosition);
            spotLightEffect.Parameters["SpotlightPosition"].SetValue(position);
            spotLightEffect.Parameters["SpotlightColor"].SetValue(color.ToVector3());
            spotLightEffect.Parameters["SpotlightDirection"].SetValue(direction);
            spotLightEffect.Parameters["SpotlightConeAngle"].SetValue(coneAngle);
            spotLightEffect.Parameters["SpotlightDecayExponent"].SetValue(decayExponent);
            spotLightEffect.Parameters["InvertedViewProjection"].SetValue(Matrix.Invert(view * projection));
            spotLightEffect.Parameters["adjust"].SetValue(screenAdjustment);
            spotLightEffect.Parameters["colorMap"].SetValue(colorRt.GetTexture());
            spotLightEffect.Parameters["normalMap"].SetValue(normalRt.GetTexture());
            spotLightEffect.Parameters["depthMap"].SetValue(depthRt.GetTexture());

            //xz scale
            float xz_scale = (float)Math.Tan(coneAngle) * radius;
            Matrix coneWorld = Matrix.CreateScale(xz_scale, radius, xz_scale) * Matrix.CreateTranslation(position);

            spotLightEffect.Parameters["world"].SetValue(coneWorld);
            spotLightEffect.Parameters["view"].SetValue(view);
            spotLightEffect.Parameters["projection"].SetValue(projection);

            //calculate the length from camera to the light center
            float distance = Vector3.Distance(camPosition, position);
            //if we are inside the light volume then we draw the interior of the sphere
            if (distance < radius)
                graphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            else
                graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            //also disable the depth buffer (we don't actually draw anything into the scene.)
            graphicsDevice.RenderState.DepthBufferEnable = false;

            //prepare to draw the sphere (assume only one meshpart)
            ModelMesh mesh = coneModel.Meshes[0];
            ModelMeshPart part = mesh.MeshParts[0];

            pointLightEffect.Begin();
            pointLightEffect.CurrentTechnique.Passes[0].Begin();

            graphicsDevice.VertexDeclaration = part.VertexDeclaration;
            graphicsDevice.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
            graphicsDevice.Indices = mesh.IndexBuffer;
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex,
                part.PrimitiveCount);

            pointLightEffect.CurrentTechnique.Passes[0].End();
            pointLightEffect.End();

            //restore back the cullmode and depth buffer
            graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            graphicsDevice.RenderState.DepthBufferEnable = true;
        }

        /// <summary>
        /// Set model config to its scene.
        /// </summary>
        /// <param name="loadedModel">Loaded model.</param>
        public void SetModelConfigForScene(ModelXMLConfig modelConfig)
        {
            scene.ModelConfig = modelConfig;
        }

        /// <summary>
        /// Set whether to draw in wireframe mode.
        /// </summary>
        /// <param name="enabled"></param>
        /// <remarks>Not work now.</remarks>
        public void SetWireFrameDrawMode(bool enabled)
        {
            if (enabled)
                graphicsDevice.RenderState.FillMode = FillMode.WireFrame;
            else
                graphicsDevice.RenderState.FillMode = FillMode.Solid;
        }

        /// <summary>
        /// Get scene currently active.
        /// </summary>
        /// <returns></returns>
        public Scene GetScene()
        {
            return scene;
        }
    }
}