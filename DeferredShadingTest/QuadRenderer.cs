﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DeferredShadingTest
{
    public class QuadRenderer
    {
        protected GraphicsDevice graphicsDevice;

        #region Vertex Infomation

        VertexDeclaration vertexDeclaration = null;
        VertexPositionTexture[] vertices = null;
        short[] indices = null;

        #endregion

        public QuadRenderer(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;

            //create vertex declaration
            vertexDeclaration = new VertexDeclaration(this.graphicsDevice, VertexPositionTexture.VertexElements);

            //create the vertices for full-screen quad
            vertices = new VertexPositionTexture[4];
            vertices[0] = new VertexPositionTexture(new Vector3(1, -1, 1), new Vector2(1, 1));
            vertices[1] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(0, 1));
            vertices[2] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(0, 0));
            vertices[3] = new VertexPositionTexture(new Vector3(1, 1, 1), new Vector2(1, 0));

            //set up the indices
            indices = new short[] { 0, 1, 2, 2, 3, 0 };
        }

        public void Draw()
        {
            graphicsDevice.VertexDeclaration = vertexDeclaration;

            graphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList,
                vertices, 0, 4, indices, 0, 2);
        }
    }
}
