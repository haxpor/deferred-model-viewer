using System;
using System.Runtime.InteropServices;

namespace DeferredShadingTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // note (System.IntPtr)0
            using (Game1 game = new Game1((System.IntPtr)0))
            {
                game.Run();
            }
        }
    }
}

