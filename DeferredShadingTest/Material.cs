﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ContentShared;

namespace DeferredShadingTest
{
    public class Material
    {
        protected GraphicsDevice graphicsDevice;
        protected ContentManager content;
        /// <summary>
        /// Effect file first send to this class will be copied and all its values will be initially set with SetValue() method.
        /// </summary>
        protected Effect materialEffect;

        //textured being used throughout the effect
        protected Texture2D diffuseTexture = null;
        protected Texture2D specularTexture = null;
        protected Texture2D normalTexture = null;

        protected int meshIndex;
        protected int boneIndex;

        //params set via effect's parameters
        protected float specularIntensity;
        protected float specularPower;
        protected Vector4 specularColor;
        protected Vector4 colorValue;
        protected float textureCoordU;
        protected float textureCoordV;

        //Effect's params
        protected EffectParameter materialColorValueEffectParam;
        protected EffectParameter diffuseTextureEffectParam;
        protected EffectParameter diffuseTextureEnabledEffectParam;
        protected EffectParameter specularTextureEffectParam;
        protected EffectParameter specularTextureEnabledEffectParam;
        protected EffectParameter specularIntensityEffectParam;
        protected EffectParameter specularPowerEffectParam;
        protected EffectParameter specularColorEffectParam;
        protected EffectParameter normalTextureEffectParam;
        protected EffectParameter normalTextureEnabledEffectParam;
        protected EffectParameter textureCoordUEffectParam;
        protected EffectParameter textureCoordVEffectParam;
        protected EffectParameter worldEffectParam;

        //Current model's mesh and meshpart (these objects will be set in BeginBatchDraw() method.
        protected ModelMesh currentMesh;
        protected ModelMeshPart currentMeshPart;

        public Material(GraphicsDevice graphicsDevice, ContentManager content, Effect effect, int meshIndex, int boneIndex)
        {
            //play safe here
            if (graphicsDevice == null)
                throw new ArgumentNullException("graphicsDevice");
            this.graphicsDevice = graphicsDevice;

            if (content == null)
                throw new ArgumentNullException("content");
            this.content = content;

            if (effect == null)
                throw new ArgumentNullException("effect");

            this.meshIndex = meshIndex;
            this.boneIndex = boneIndex;

            //clone the effect for this material
            materialEffect = effect.Clone(graphicsDevice);

            //get the effect's params (reduce the string look-up)
            materialColorValueEffectParam = materialEffect.Parameters["materialColor"];
            diffuseTextureEnabledEffectParam = materialEffect.Parameters["diffuseTextureEnabled"];
            diffuseTextureEffectParam = materialEffect.Parameters["diffuseTexture"];
            specularTextureEffectParam = materialEffect.Parameters["specularTexture"];
            specularTextureEnabledEffectParam = materialEffect.Parameters["specularTextureEnabled"];
            specularIntensityEffectParam = materialEffect.Parameters["specularIntensity"];
            specularPowerEffectParam = materialEffect.Parameters["specularPower"];
            specularColorEffectParam = materialEffect.Parameters["specularColor"];
            normalTextureEffectParam = materialEffect.Parameters["normalTexture"];
            normalTextureEnabledEffectParam = materialEffect.Parameters["normalTextureEnabled"];
            textureCoordUEffectParam = materialEffect.Parameters["texCoordU"];
            textureCoordVEffectParam = materialEffect.Parameters["texCoordV"];
            worldEffectParam = materialEffect.Parameters["world"];
        }

        #region Properties
        /// <summary>
        /// Get mesh index used in drawing process for this material.
        /// </summary>
        public int MeshIndex
        {
            get { return meshIndex;  }
        }
        /// <summary>
        /// Get bone index used in drawing process for this material.
        /// </summary>
        public int BoneIndex
        {
            get { return boneIndex; }
        }
        #endregion Properties

        #region Material Properties

        /// <summary>
        /// Set or get the diffuse texture.
        /// </summary>
        public Texture2D DiffuseTexture
        {
            set
            {
                diffuseTexture = value;
                if (diffuseTexture == null)
                {
                    diffuseTextureEnabledEffectParam.SetValue(false);
                    diffuseTextureEffectParam.SetValue((Texture)null);
                }
                else
                {
                    diffuseTextureEnabledEffectParam.SetValue(true);
                    diffuseTextureEffectParam.SetValue(diffuseTexture);
                }
            }

            get { return diffuseTexture; }
        }

        /// <summary>
        /// Set or get the specular texture.
        /// </summary>
        public Texture2D SpecularTexture
        {
            set
            {
                specularTexture = value;
                if (specularTexture == null)
                {
                    specularTextureEnabledEffectParam.SetValue(false);
                    specularTextureEffectParam.SetValue((Texture2D)null);
                }
                else
                {
                    specularTextureEnabledEffectParam.SetValue(true);
                    specularTextureEffectParam.SetValue(specularTexture);
                }
            }

            get { return specularTexture; }
        }

        /// <summary>
        /// Set or get the normal texture.
        /// </summary>
        public Texture2D NormalTexture
        {
            set
            {
                normalTexture = value;
                if (normalTexture == null)
                {
                    normalTextureEnabledEffectParam.SetValue(false);
                    normalTextureEffectParam.SetValue((Texture2D)null);
                }
                else
                {
                    normalTextureEnabledEffectParam.SetValue(true);
                    normalTextureEffectParam.SetValue(normalTexture);
                }
            }

            get { return normalTexture; }
        }

        /// <summary>
        /// Set or get the specular intensity.
        /// </summary>
        public float SpecularIntensity
        {
            set
            {
                specularIntensity = value;
                specularIntensityEffectParam.SetValue(specularIntensity);
            }

            get { return specularIntensity; }
        }

        /// <summary>
        /// Set or get the specular power.
        /// </summary>
        public float SpecularPower
        {
            set {  
                specularPower = value;
                specularPowerEffectParam.SetValue(specularPower);
            }

            get { return specularPower; }
        }

        /// <summary>
        /// Set or get the specular color.
        /// </summary>
        public Vector4 SpecularColor
        {
            set { 
                specularColor = value;
                specularColorEffectParam.SetValue(specularColor);
            }
            get { return specularColor; }
        }

        /// <summary>
        /// Set or get the material's color.
        /// </summary>
        /// <remarks>If not set, will use white color.</remarks>
        public Vector4 MaterialColor
        {
            set
            {
                colorValue = value;
                //if colorValue is valid, we send the new value to GPU (default is white)
                if (colorValue != null)
                {
                    materialColorValueEffectParam.SetValue(colorValue);
                }
            }
            get { return colorValue; }
        }

        /// <summary>
        /// Set or get the texture's coordinate U.
        /// </summary>
        /// <remarks>If not set, then will use 1.0.</remarks>
        public float TextureCoordU
        {
            set
            {
                if (value > 0.0f)
                {
                    textureCoordU = value;
                    textureCoordUEffectParam.SetValue(textureCoordU);
                }
            }

            get { return textureCoordU; }
        }

        /// <summary>
        /// Set or get the texture's coordinate V.
        /// </summary>
        /// <remarks>If not set, then will use 1.0.</remarks>
        public float TextureCoordV
        {
            set
            {
                if (value > 0.0f)
                {
                    textureCoordV = value;
                    textureCoordVEffectParam.SetValue(textureCoordV);
                }
            }

            get { return textureCoordV; }
        }

        #endregion

        /// <summary>
        /// Draw with all mesh parts.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="model"></param>
        /// <param name="world"></param>
        public void DrawFor(GameTime gameTime, Model model, Matrix world)
        {
            Matrix[] boneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            ModelMesh currentMesh = model.Meshes[meshIndex];
            foreach (ModelMeshPart currentMeshPart in currentMesh.MeshParts)
            {
                //set up the geometry buffer and its properties
                graphicsDevice.Vertices[0].SetSource(currentMesh.VertexBuffer, currentMeshPart.StreamOffset, currentMeshPart.VertexStride);
                graphicsDevice.Indices = currentMesh.IndexBuffer;
                graphicsDevice.VertexDeclaration = currentMeshPart.VertexDeclaration;

                //set the world matrix for this model
                if(boneIndex < 0)
                    worldEffectParam.SetValue(boneTransforms[currentMesh.ParentBone.Index] * world);
                else
                    worldEffectParam.SetValue(boneTransforms[boneIndex] * world);

                //begin effect
                materialEffect.Begin();

                //begin the Ambient pass
                materialEffect.CurrentTechnique.Passes[0].Begin();

                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, currentMeshPart.BaseVertex, 0,
                    currentMeshPart.NumVertices, currentMeshPart.StartIndex,
                    currentMeshPart.PrimitiveCount);

                materialEffect.CurrentTechnique.Passes[0].End();

                materialEffect.End();
            }
        }
    }
}
