// Used to extract the bright pixel out from the input texture.
// This effect is used for post-processing.

//this sampler will come from the input parameter of the draw method of spritebatch.
sampler sceneSampler : register(s0);

float BloomThreshold;

float4 PixelShader(float2 texCoord : TEXCOORD0) : COLOR0
{
	//look up the color from the texture
	float4 color = tex2D(sceneSampler, texCoord);
	
	//extract the bright pixel (only those that higher than BloomThreshold will still hold)
	return saturate( (color - BloomThreshold) / (1 - BloomThreshold) );
}

technique BloomExtract
{
	pass P0
	{
		PixelShader = compile ps_2_0 PixelShader();
	}
}

