float4x4 world;
float4x4 view;
float4x4 projection;

//position of the camera
float3 CameraPosition; 

//color of the light
float3 LightColor;

//position of the light
float3 LightPosition;

//light intensity
float LightIntensity;

//light radius
float LightRadius;

//inverted view-projection, used to compute the world position
float4x4 InvertedViewProjection;

//adjustment of the pixel
float2 adjust;

//diffuse color, and spcular intensity
texture colorMap;
//normal, and specular power
texture normalMap;
//depth
texture depthMap;

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct VertexShaderInput
{
	float3 Position: POSITION0;
};

struct VertexShaderOutput
{
	float4 Position: POSITION0;
	float4 ScreenPosition: TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	//calculate the wvp matrix
	float4x4 wvp = mul(world, mul(view, projection) );
	
	//calculate the screen-position
	output.Position = mul( float4(input.Position, 1.0f), wvp);
	//make a copy of the screen-position
	output.ScreenPosition = output.Position;
	
	return output;
}

float4 PixelShaderFunction(VertexShaderOutput input): COLOR0
{
	//get the screen position
	input.ScreenPosition.xy /= input.ScreenPosition.w;
	
	//get the texture coordinate from the screen position
	//Note: The texture coordinate is in [0,1]*[0,1]
	//The screen coordinate is in [-1,1] * [1,-1]
	float2 texCoord = 0.5f * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1) - adjust;
	
	//get the normal data from the normal map
	float4 normalData = tex2D(normalMapSampler, texCoord);
	//transform normal back into [-1,1]
	float3 normal = 2.0f * normalData.xyz - 1.0f;
	//get specular power, by get in the range of [0,255]
	float specularPower = normalData.a * 255;
	//get the specular intensity
	float specularIntensity = tex2D(colorMapSampler, texCoord).a;
	
	//get depth
	float depth = tex2D(depthMapSampler, texCoord).r;
	
	//calculate the screen space position
	float4 position;
	position.xy = input.ScreenPosition.xy;
	position.z = depth;
	position.w = 1.0f;
	//transform to the world space
	position = mul(position, InvertedViewProjection);
	position /= position.w;
	
	//calculate the light part
	//calculate the light vector
	float3 lightVector = LightPosition - position;
	
	//calculate the attenuation
	float attenuation = saturate(1.0f - length(lightVector) / LightRadius);
	
	//normalize the light vector
	lightVector = normalize(lightVector);
	
	//calculate the diffuse light
	float NdL = max(0, dot(normal, lightVector));
	float3 diffuseLight = NdL * LightColor.rgb;
	
	//calculate the reflection vector
	float3 reflectionVector = normalize(reflect(-lightVector, normal));
	//calculate the direction to camera vector
	float3 directionToCameraVector = normalize(CameraPosition - position);
	
	//calculate the specular light
	float specularLight = specularIntensity * pow( saturate(dot(reflectionVector, directionToCameraVector)), specularPower);
	
	return attenuation * LightIntensity * float4(diffuseLight.rgb, specularLight);
}

technique PointLight
{
	pass P0
	{
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
	}
}
