//This effect is used to combine the color from the mesh's texture and the light map which drawed prior to this effect.

sampler sceneSampler : register (s0);
sampler lightSampler : register (s1);

float4 CombinePS(float2 texCoord: TEXCOORD0): COLOR0
{
	float3 diffuseColor = tex2D(sceneSampler, texCoord);
	float4 lightData = tex2D(lightSampler, texCoord);
	
	float3 lightColor = lightData.rgb;
	float specular = lightData.a;
	
	return float4(((diffuseColor * lightColor) + specular), 1);
}

technique CombineLight
{
	pass P0
	{
		PixelShader = compile ps_2_0 CombinePS();
	}
}