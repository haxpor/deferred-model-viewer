///wvp matrix
shared float4x4 world;
shared float4x4 view;
shared float4x4 projection;

//camera properties
shared float3 cameraPosition;
//light properties
shared float4 ambientLightColor;
shared float4 specularLightColor;

//fog properties
shared float fogNear = 50;
shared float fogFar = 400;
shared float fogAltitudeScale = 30;
shared float fogThinning = 10;
shared float4 fogColor = {0.5, 0.5, 0.5, 1.0};
shared bool fogEnabled = false;

struct Light
{
	float3 Position;
	float3 LookAt;
	float4 Color;
};

shared Light light;

float4 materialColor = float4(1,1,1,1);
float specularIntensity = 1.0f;
float specularPower = 1.0f;
float4 specularColor;
bool diffuseTextureEnabled = false;
bool specularTextureEnabled = false;
bool normalTextureEnabled = false;

//texture coordinate
float texCoordU = 1.0f;
float texCoordV = 1.0f;

//sampler states
texture2D diffuseTexture;
texture2D specularTexture;
texture2D normalTexture;

sampler diffuseSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <diffuseTexture>;
};

sampler specularSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <specularTexture>;
};

sampler normalSampler = sampler_state
{
	MipFilter = Linear;
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
	texture = <normalTexture>;
};

struct BasicVSOutput
{
	float4 Position: POSITION;
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
};

struct BasicPSInput
{
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
};

// ## List of helper functions ## //
// Include the calculate from the normal texture
float4 CalculateSingleLight(float3 worldPosition, float3 worldNormal, 
                            float4 diffuseColor, float4 specularIntensityColor, float3 normCVector)
{
     float3 lightVector = light.Position - worldPosition;
     float3 directionToLight = normalize(lightVector);
     
     float diffuseIntensity = saturate( dot(directionToLight, normCVector));
     float4 diffuse = diffuseIntensity * light.Color * diffuseColor;

     //calculate Phong components per-pixel
     float3 reflectionVector = normalize(reflect(-directionToLight, normCVector));
     float3 directionToCamera = normalize(cameraPosition - worldPosition);
     
     //calculate specular component
     float4 specular = saturate(light.Color * specularColor * specularIntensity * specularIntensityColor * specularLightColor *
                       pow(saturate(dot(reflectionVector, directionToCamera)), 
                           specularPower));
                           
     return  diffuse + specular;
}

float4 CalculateSingleLight(float3 worldPosition, float3 worldNormal, 
                            float4 diffuseColor, float4 specularIntensityColor)
{
     float3 lightVector = light.Position - worldPosition;
     float3 directionToLight = normalize(lightVector);
     
     float diffuseIntensity = saturate( dot(directionToLight, worldNormal));
     float4 diffuse = diffuseIntensity * light.Color * diffuseColor;

     //calculate Phong components per-pixel
     float3 reflectionVector = normalize(reflect(-directionToLight, worldNormal));
     float3 directionToCamera = normalize(cameraPosition - worldPosition);
     
     //calculate specular component
     float4 specular = saturate(light.Color * specularColor * specularIntensity * specularIntensityColor * specularLightColor *
                       pow(saturate(dot(reflectionVector, directionToCamera)), 
                           specularPower));
                           
     return  diffuse + specular;
}

// FogPS
// Calculate the effect of fog.
// It will interpolates the input color with fog and return the interpolated color.
float4 FogPS(float4 color, float3 worldPosition) : COLOR
{
    float d = length(worldPosition - cameraPosition);    
    float l = saturate((d - fogNear) / (fogFar - fogNear) / clamp(worldPosition.y / fogAltitudeScale + 1, 1, fogThinning));
    
    return lerp(color, fogColor, l);
}

// ## End of helper functions ## //

// BasicVS()
BasicVSOutput BasicVS(float3 position: POSITION, float3 normal: NORMAL, float2 texCoords: TEXCOORD)
{
	BasicVSOutput output;
	
	//generate the wvp-matrix
	float4x4 wvp = mul( mul( world, view), projection);
	
	//get the position on screen
	output.Position = mul( float4(position, 1.0f), wvp);
	
	//transform the position into world space
	float4 worldPosition = mul( float4( position, 1.0f), world);
	output.WorldPosition = worldPosition / worldPosition.w;
	
	//transform the normal into world space
	output.WorldNormal = mul( normal, world);
	
	//copy the texture's coordinate
	output.TexCoords.x = texCoords.x * texCoordU;
	output.TexCoords.y = texCoords.y * texCoordV;
	
	return output;
}

// BasicPS()
// It is the basic pixel shader function that calculates the diffuse color, with specular color affected by only 1 light.
float4 BasicPS(BasicPSInput input): COLOR
{
	float4 diffuseColor = materialColor;
	float4 specularIntensityColor = materialColor;
	float3 normalCVector;
	
	if(diffuseTextureEnabled)
	{
		diffuseColor = tex2D(diffuseSampler, input.TexCoords);
	}
	if(specularTextureEnabled)
	{
		specularIntensityColor = tex2D(specularSampler, input.TexCoords);
	}
	if(normalTextureEnabled)
	{
		normalCVector = tex2D(normalSampler, input.TexCoords);
		//normalCVector = normalize(normalCVector);
	}
	
	//calculate the ambient light
	float4 color = ambientLightColor * diffuseColor;
	
	if(normalTextureEnabled)
		color += CalculateSingleLight(input.WorldPosition, input.WorldNormal, diffuseColor, specularIntensityColor, normalCVector);
	else
		color += CalculateSingleLight(input.WorldPosition, input.WorldNormal, diffuseColor, specularIntensityColor);
	
	//calculate the effect of fog (reuse color variable)
	if(fogEnabled)
		color = FogPS(color, input.WorldPosition);
		
	//only opaque color here
	color.a = 1.0f;
	
	return color;
}