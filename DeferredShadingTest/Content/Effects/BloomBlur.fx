// This effect is used to blur the original scene texture.
// This is the very dirty and quick version of gaussian blur.

sampler sceneSampler : register(s0);

//Width to sampling-pixels
float blurMag;

//Alpha value
float alpha;

const float2 offsets[12] = {
   -0.326212, -0.405805,
   -0.840144, -0.073580,
   -0.695914,  0.457137,
   -0.203345,  0.620716,
    0.962340, -0.194983,
    0.473434, -0.480026,
    0.519456,  0.767022,
    0.185461, -0.893124,
    0.507431,  0.064425,
    0.896420,  0.412458,
   -0.321940, -0.932615,
   -0.791559, -0.597705,
};

float4 PixelShader(float2 texCoord : TEXCOORD0) : COLOR0
{
	//look up the color from the texture
	float4 sum = tex2D(sceneSampler, texCoord);
	
	//accumulate from the 12-kernel
	for(int i=0; i<12; i++)
	{
		sum += tex2D(sceneSampler, texCoord + blurMag * offsets[i]);
	}
	
	//average them
	sum /= 13;
	
	return sum;
}

technique BloomBlur
{
	pass P0
	{
		PixelShader = compile ps_2_0 PixelShader();
	}
}