// This effect is used to combine the original texture with the bloom one.

sampler sceneSampler : register(s0);
sampler postSampler : register(s1);

float sceneIntensity;
float bloomIntensity;

float sceneSaturation;
float bloomSaturation;

// Helper for modifying the saturation of a color.
float4 AdjustSaturation(float4 color, float saturation)
{
    // The constants 0.3, 0.59, and 0.11 are chosen because the
    // human eye is more sensitive to green light, and less to blue.
    float grey = dot(color, float3(0.3, 0.59, 0.11));

    return lerp(grey, color, saturation);
}

float4 PixelShader(float2 texCoord: TEXCOORD): COLOR0
{
	float4 scene = tex2D(sceneSampler, texCoord);
	float4 bloom = tex2D(postSampler, texCoord);
	
	//adjust color saturation and intensity
	scene = AdjustSaturation(scene, sceneSaturation) * sceneIntensity;
	bloom = AdjustSaturation(bloom, bloomSaturation) * bloomIntensity;
	
	//darken the scene image where there is a lot of bloom
	//this will avoid the too much of burn-out effect
	scene *= (1 - (saturate(bloom)-0.0001));
	
	return scene + bloom;
}

technique CombineFinal
{
	pass P0
	{
		PixelShader = compile ps_2_0 PixelShader();
	}
}