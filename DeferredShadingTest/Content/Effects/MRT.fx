#include "Includes.inc"

struct MRT_PSOutput
{
	float4 Color: COLOR0;
	float4 Color1: COLOR1;
	float4 Color2: COLOR2;
	float4 Color3: COLOR3;
};

MRT_PSOutput MRTPS(BasicPSInput input)
{
	MRT_PSOutput output;
	
	//color#1 Output depth
	output.Color = BasicPS(input);
	output.Color1 = float4(input.WorldNormal * 256, 1);
	output.Color2 = float4(0,1,0,1);
	output.Color3 = float4(0,0,1,1);
	
	return output;
}

technique MRT
{
	pass p0
	{
		VertexShader = compile vs_2_0 BasicVS();
		PixelShader = compile ps_3_0 MRTPS();
	}
}