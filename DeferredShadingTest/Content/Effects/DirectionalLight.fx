//Note that the following attributes will not be messed up within Includes.inc file.
// This scheme will separate the directional light to different file.
// Also note that this is the 2d postprocessing, so the vertex transformation will not be performed, just route it to the pixel shader.

//screen adjust
shared float2 adjust = 0.0f;

//direction of the light
float3 lightDirection;

//color of the light
float3 lightColor;

//camera position
float3 cameraPosition;

//invert of view-projection matrix
float4x4 invertedViewProjection;

//textures set
//diffuse color, and specular intensity in the alpha channel
texture colorMap;
//normals, and specular power in the alpha channel
texture normalMap;
//depth
texture depthMap;

//use for directional light only
struct DirectionalLight
{
	float3 Position;
	float3 LookAt;
	float4 Color;
};

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct DirectionalLightVertexOutput
{
	float4 Position: POSITION0;
	float2 TexCoords: TEXCOORD0;
};

DirectionalLightVertexOutput DirectionalLightVS(float3 position: POSITION0, float2 texCoords: TEXCOORD0)
{
	DirectionalLightVertexOutput output;
	
	output.Position = float4(position, 1.0f);
	output.TexCoords = texCoords;
	
	return output;
}

float4 DirectionalLightPS(DirectionalLightVertexOutput input): COLOR0
{
	//get the normal data from normal map
	float4 normalData = tex2D(normalMapSampler, input.TexCoords);
	//transfrom back to [-1,1]
	float3 normal = 2.0f * normalData.rgb - 1.0f;
	//get the specular power, and convert it into [0,255]
	float specularPower = normalData.a * 255;
	
	//get specular intensity
	float specularIntensity = tex2D(colorMapSampler, input.TexCoords).a;
	
	//read depth
	float depthVal = tex2D(depthMapSampler, input.TexCoords).r;
	//create the screen position
	float4 position;
		//get x position by convert it from [0,1] to [-1,1]
	position.x = input.TexCoords.x * 2.0f - 1.0f;
		//same for y but with different sign
	position.y = -(input.TexCoords.y * 2.0f - 1.0f);
	position.z = depthVal;
	position.w = 1.0f;
	
	//trasform it into the world space
	position = mul(position, invertedViewProjection);
	position /= position.w;
		//now the position is in the world space
	
	//calculate the direction to light
	float3 directionToLight = -normalize(lightDirection);
	
	//calculate the lambertian light
	float NdL = max(0, dot(normal, directionToLight));
	float3 diffuseLight = NdL * lightColor.rgb;
	
	//calculate the reflection vector
	float3 reflectVector = normalize(reflect(-directionToLight, normal));
	//calculate the direction to camera
	float3 directionToCamera = normalize(cameraPosition - position);
	//calculate the specular light
	float specularLight = specularIntensity * pow( saturate( dot(reflectVector, directionToCamera) ), specularPower);
	
	//output the result color
	return float4(diffuseLight.rgb, specularLight);
}

technique DeferredDirectionalLight
{
	pass p0
	{
		VertexShader = compile vs_2_0 DirectionalLightVS();
		PixelShader = compile ps_2_0 DirectionalLightPS();
	}
}