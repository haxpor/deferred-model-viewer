float4x4 world;
float4x4 view;
float4x4 projection;

//position of the camera
float3 CameraPosition; 

//color of the light
float3 SpotlightColor;

//position of the light
float3 SpotlightPosition;

//spotlight direction
float3 SpotlightDirection;

//spotlight radius
float SpotlightRadius;

//spotlight cone angle
float SpotlightConeAngle;
//spotlight decay exponent
float SpotlightDecayExponent;

//inverted view-projection, used to compute the world position
float4x4 InvertedViewProjection;

//adjustment of the pixel
float2 adjust;

//diffuse color, and spcular intensity
texture colorMap;
//normal, and specular power
texture normalMap;
//depth
texture depthMap;

sampler colorMapSampler = sampler_state
{
	Texture = <colorMap>;
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};

sampler normalMapSampler = sampler_state
{
	Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler depthMapSampler = sampler_state
{
	Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct VertexInput
{
	float3 Position: POSITION0;
};

struct VertexOutput
{
	float4 Position: POSITION0;
	float4 ScreenPosition: TEXCOORD0;
};

VertexOutput VertexShaderFunction(VertexInput input)
{
	VertexOutput output;
	
	//generate the wvp matrix
	float4x4 wvp = mul( world, mul( view, projection) );
	
	//transform into the screen position
	output.Position = mul(float4(input.Position, 1.0f), wvp);
	//make a copy of the screen position
	output.ScreenPosition = output.Position;
	
	return output;
}

float4 PixelShaderFunction(VertexOutput input): COLOR0
{
	//get the screen position
	input.ScreenPosition.xy /= input.ScreenPosition.w;
	
	//calculate the texture coordinate
	//Note: Transform from screen coordinate ([-1,1] * [1,-1]) into texture coordiante ([0,1] * [0,1]).
	float2 texCoord = 0.5f * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1.0f) - adjust;
	
	//get the normal data
	float4 normalData = tex2D(normalMapSampler, texCoord);
	//get the normal
	float3 normal = normalData.xyz;
	//transform the normal from color range [0,1] into normal range [-1,1]
	normal = 2.0f * normal - 1.0f;
	//get the specular power, and convert it into range [0,255]
	float specularPower = normalData.a * 255;
	//get the specular intensity
	float specularIntensity = tex2D(colorMapSampler, texCoord).a;
	
	//get the depth
	float depth = tex2D(depthMapSampler, texCoord).r;
	
	//create the world position
	float4 position;
	position.xy = input.ScreenPosition.xy;
	position.z = depth;
	position.w = 1.0f;
	//transform into world position
	position = mul(position, InvertedViewProjection);
	position /= position.w;
	
	//calculate the light part
	//calculate the light direction
	float3 lightDirectionVector = SpotlightPosition - position;
	
	//calculate the attenuation
	float attenuation = saturate(1.0f - length(lightDirectionVector)/SpotlightRadius);
	
	//normalize the light direction vector
	lightDirectionVector = normalize(lightDirectionVector);
	
	//if in the range of spotlight
	float SdL = dot(SpotlightDirection, lightDirectionVector);
	if(SdL < SpotlightConeAngle)
	{
		//calculate the spotlight intensity
		float spotlightIntensity = pow(SdL, SpotlightDecayExponent);
		
		//calculate the diffuse light
		float NdL = max(0, dot(normal, lightDirectionVector));
		float3 diffuseLight = NdL * SpotlightColor.rgb;
		
		//calculate the reflection vector
		float3 reflectionVector = normalize(reflect(-lightDirectionVector, normal));
		//calculate the direction to camera vector
		float3 directionToCameraVector = normalize(CameraPosition - position);
		
		//calculate the specular light
		float specularLight = specularIntensity * pow( saturate(dot(reflectionVector, directionToCameraVector)), specularPower);
		
		return attenuation * spotlightIntensity * float4(diffuseLight.rgb, specularLight);
	}
	else
	{
		return float4(0,0,0,0);
	}
	
}

technique Spotlight
{
	pass P0
	{
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
	}
}