#include "Includes.inc"

struct RenderGBufferVertexOutput
{
	float4 Position: POSITION;
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 Depth: TEXCOORD3;
	float3x3 TangentToWorld: TEXCOORD4;
};

struct RenderGBufferPixelInput
{
	float3 WorldPosition: TEXCOORD0;
	float3 WorldNormal: TEXCOORD1;
	float2 TexCoords: TEXCOORD2;
	float2 Depth: TEXCOORD3;
	float3x3 TangentToWorld: TEXCOORD4;
};

struct RenderGBufferPixelOutput
{
	float4 Color: COLOR0;
	float4 Normal: COLOR1;
	float4 Depth: COLOR2;
};

RenderGBufferVertexOutput RenderGBufferVS(float3 position: POSITION, float3 normal: NORMAL, float3 binormal: BINORMAL0, float3 tangent: TANGENT0
	, float2 texCoords: TEXCOORD)
{
	RenderGBufferVertexOutput output;
	
	//generate the wvp-matrix
	float4x4 wvp = mul( mul( world, view), projection);
	
	//get the position on screen
	output.Position = mul( float4(position, 1.0f), wvp);
	//get the depth information
	output.Depth = float2(output.Position.z, output.Position.w);
	
	//transform the position into world space
	float4 worldPosition = mul( float4( position, 1.0f), world);
	output.WorldPosition = worldPosition / worldPosition.w;
	
	// transform normal into world space (use this in pixel shader in case of the model doesn't have the separate normal map file)
	output.WorldNormal = mul(normal, world);
	
	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors
    output.TangentToWorld[0] = mul(tangent, world);
    output.TangentToWorld[1] = mul(binormal, world);
    output.TangentToWorld[2] = mul(normal, world);
	
	//copy the texture's coordinate
	output.TexCoords.x = texCoords.x * texCoordU;
	output.TexCoords.y = texCoords.y * texCoordV;
	
	return output;
}

RenderGBufferPixelOutput RenderGBufferPS(RenderGBufferPixelInput input)
{
	RenderGBufferPixelOutput output;
	
	//Color
	//get the color information from diffuse texture
	output.Color = tex2D(diffuseSampler, input.TexCoords);
	//set the specular intensity
	output.Color.a = specularIntensity;
	
	//Normal
	//get the normal from texture if available
	if(normalTextureEnabled)
	{
		//get the normal data from the normal texture (of the model)
		float3 normal = tex2D(normalSampler, input.TexCoords).rgb;
		//transform into [-1,1]
		normal = 2.0f * normal - 1.0f;
		//transform it with TangentToWorld matrix
		normal = mul(normal, input.TangentToWorld);
		//normalize the result
		normal = normalize(normal);
		//convert back into [0,1]
		output.Normal.rgb = 0.5f * (normal + 1.0f);
	}
	else{
		//transform from [-1,1] to [0,1]
		output.Normal.rgb = 0.5f * (normalize(input.WorldNormal) + 1.0f);
	}
	//set the specular power
	output.Normal.a = specularPower;
	
	//Depth
	output.Depth = input.Depth.x / input.Depth.y;
	return output;
}

technique RenderGBuffer
{
	pass p0
	{
		VertexShader = compile vs_2_0 RenderGBufferVS();
		PixelShader = compile ps_3_0 RenderGBufferPS();
	}
}