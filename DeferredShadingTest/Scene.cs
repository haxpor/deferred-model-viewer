﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ContentShared;

namespace DeferredShadingTest
{
    /// <summary>
    /// Purpose of scene class is to separate the content from the logic code.
    /// </summary>
    public class Scene
    {
        /// <summary>
        /// GraphicsDevice from game class.
        /// </summary>
        GraphicsDevice graphicsDevice;

        /// <summary>
        /// ContentManager from game class.
        /// </summary>
        ContentManager content;

        /// <summary>
        /// Effect file used for deferred shading.
        /// </summary>
        Effect renderGBufferEffect;

        /// <summary>
        /// Game instance.
        /// </summary>
        Game1 game;

        #region Model Config
        /// <summary>
        /// Model to draw in this scene.
        /// </summary>
        Model model;
        Material[] modelMaterials;  // for all meshes in the model
        ModelXMLConfig modelConfig;
        /// <summary>
        /// Get the current model
        /// </summary>
        public Model CurrentModel
        {
            get { return model; }
        }

        public ModelXMLConfig ModelConfig
        {
            get
            {
                return modelConfig;
            }
            set
            {
                modelConfig = value;

                // set to scene's model as well
                if (modelConfig.Tag == null)
                {
                    model = null;

                    // null out the materials
                    modelMaterials = null;
                }
                else
                {
                    model = ((UserTagModelLevel)modelConfig.Tag).Model;

                    // set up all materials for all mesh
                    modelMaterials = new Material[modelConfig.Meshes.Count];
                    for (int i = 0; i < modelConfig.Meshes.Count; i++)
                    {
                        // get mesh
                        Mesh m = modelConfig.Meshes[i];
                        // get mesh's tag
                        UserTagMeshLevel tag = (UserTagMeshLevel)m.Tag;

                        modelMaterials[i] = new Material(graphicsDevice, content, renderGBufferEffect, modelConfig.Meshes[i].Index, modelConfig.Meshes[i].BoneIndex);
                        modelMaterials[i].TextureCoordU = 1f;
                        modelMaterials[i].TextureCoordV = 1f;
                        modelMaterials[i].DiffuseTexture = tag.Diffuse;
                        modelMaterials[i].NormalTexture = tag.Normal;
                        modelMaterials[i].SpecularTexture = tag.Specular;
                    }
                }
            }
        }

        #endregion Model Config

        public Scene(Game1 game, GraphicsDevice graphicsDevice, Effect effect, ContentManager content)
        {
            this.graphicsDevice = graphicsDevice;
            this.renderGBufferEffect = effect;
            this.content = content;
            this.game = game;

            model = null;
            modelMaterials = null;
            modelConfig = null;

            Initialize();
        }

        private void Initialize()
        {
            // do nothing
        }

        public void Update(GameTime gameTime)
        {
        }

        public void Draw(GameTime gameTime)
        {
            graphicsDevice.RenderState.AlphaBlendEnable = false;
            graphicsDevice.RenderState.DepthBufferEnable = true;

            //draw a model (if set)
            if (model != null)
            {
                if (modelMaterials != null)
                {
                    foreach (Material m in modelMaterials)
                    {
                        m.DrawFor(gameTime, model, Matrix.CreateScale(game.GlobalModelScale));
                    }
                }
            }
        }

        public void SetUpCamera(Vector3 modelCenter, float modelRadius)
        {
            // nothing more to do here
        }
    }
}
