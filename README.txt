Deferred Model Viewer

This project is hereby governed by the license "Microsoft Permissive License" file.

I have used "ContentBuilder.cs", "ErrorLogger.cs", "ServiceContainer.cs", and sample models as part of creating this project. So be sure to read the license file before doing any actions further.

-- How to use program --
Observe "sample" folder at the root project folder to see sample models, and their xml file to load into the program.
Everytime the program loads a new model, it will center the users at the central point of the model itself, users need to find a way to be above or around the model themselves.

Short keys are as follows.
[Note]: Due to the current version didn't have a free-mouse control feature so users are forced to use short keys to access other capabilities of the program as mouse movement is reserved for model viwer functionality only.

Alt + X = Load a selected model file (.fbx, .x) to export its model structure file (.out)
Alt + O = Load a model from its config file (.xml) and show in the program
Alt + E = Exit the program
Alt + S = Change the scaling factor for the current loaded model
A,D,W,S = Camera transition movement, left, right, up, and down respectively.
Q,E 	= Fly the camera up / down
Mouse 	= Change camera's view

If you would like to contact me, feel free to hit me at haxpor {add} gmail.com.

Wasin