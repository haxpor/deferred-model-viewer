﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ContentShared;
using DeferredShadingTest;

namespace DeferredShadingModelViewer
{
    public partial class Form1 : Form
    {
        ContentBuilder contentBuilder;
        ContentManager contentManager;
        ModelStructureWriter modelWriter;
        Game1 game;
        bool isWireframeDrawMode = false;   // default is disabled

        string baseFormTitle = "";
        public static string STATUS_LOADING_MODEL_FOR_EXPORTING = "   -- status: loading model for exporting --";
        public static string STATUS_LOADING_MODEL_FOR_RENDERING = "   -- status: loading model for rendering --";
        public static string STATUS_LOADING_XML_MODEL_FILE = "   -- status: loading xml file --";
        public static string STATUS_EXPORTING_MODEL_STRUCTURE = "   -- status: exporting model's structure file --";

        /// <summary>
        /// Structure used after measurement of the model.
        /// </summary>
        struct ModelInfo
        {
            public Vector3 Center;
            public float Radius;
        };

        /// <summary>
        /// Get or set the global model scale.
        /// </summary>
        public float GlobalModelScale
        {
            get { return GlobalSettings.GLOBAL_SCALE; }
            set 
            {
                GlobalSettings.GLOBAL_SCALE = value;

                // set to game
                game.GlobalModelScale = value;
            }
        }

        public Form1()
        {
            InitializeComponent();

            // save the base title
            baseFormTitle = this.Text;

            // find the custom assemblies path
            string[] customAssemblies = {
                                            // nothing here
                                        };
            // create content builder with our custom builder
            contentBuilder = new ContentBuilder(customAssemblies);
            // -- xna game will set contentManager (not elegent :(
            contentManager = null;
            // create a model structure writer
            modelWriter = new ModelStructureWriter();

            game = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game.Exit();
            Close();
        }

        /// <summary>
        /// Helper function for assisting content builder.
        /// </summary>
        /// <returns></returns>
        /// <remarks>NOT USED ANYMORE</remarks>
        //private string findTheCurrentContentSharedDLLPath()
        //{
        //    string assemblyLocation = Path.GetDirectoryName(Path.GetFullPath(Assembly.GetExecutingAssembly().Location));
        //    string dllPath = assemblyLocation + "\\ContentShared.dll";
        //    return Path.GetFullPath(dllPath);
        //}

        /// <summary>
        /// Helper function for assisting content builder.
        /// </summary>
        /// <returns></returns>
        /// <remarks>NOT USED ANYMORE</remarks>
        //private string findTheCurrentContentPipelineDLLPath()
        //{
        //    string assemblyLocation = Path.GetDirectoryName(Path.GetFullPath(Assembly.GetExecutingAssembly().Location));
        //    string dllPath = assemblyLocation + "\\ContentPipeline.dll";
        //    return Path.GetFullPath(dllPath);
        //}

        /// <summary>
        /// Open dialog to choose .fbx file to render.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            // Default to the directory which contains our content files.
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            string relativePath = Path.Combine(assemblyLocation, "");
            string contentPath = Path.GetFullPath(relativePath);

            fileDialog.InitialDirectory = contentPath;

            // load .xml file
            fileDialog.Title = "Load XML file";

            fileDialog.Filter = "XML Files (*.xml)|*.xml|" +
                                "All Files (*.*)|*.*";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadXML(fileDialog.FileName);
            }
        }

        /// <summary>
        /// Loads a new XML file.
        /// </summary>
        private void LoadXML(string filePath)
        {
            Cursor = Cursors.WaitCursor;

            // get the filename
            string fileName = Path.GetFileName(filePath);

            // create an empty model config just for brevity
            this.game.SetModelConfig(new ModelXMLConfig());
            // Unload any existing model.
            contentManager.Unload();

            // change status
            this.Text += STATUS_LOADING_XML_MODEL_FILE;

            // Read in xml file natively
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);

            // load xml file successfully
            this.Text = baseFormTitle;

            // #1 load information into a class
            string modelName = xmlDoc.GetElementsByTagName("Name")[0].InnerText;
            string modelFilePath = xmlDoc.GetElementsByTagName("FilePath")[0].InnerText;
            float modelScale = Convert.ToSingle(xmlDoc.GetElementsByTagName("Scale")[0].InnerText);

            List<Mesh> meshes = new List<Mesh>();
            XmlNodeList meshesNode = xmlDoc.GetElementsByTagName("Mesh");

            for (int i = 0; i < meshesNode.Count; i++)
            {
                // get a single node
                XmlNode meshNode = meshesNode[i];

                // get info
                int index = Convert.ToInt32(meshNode["Index"].InnerText);
                int boneIndex = Convert.ToInt32(meshNode["BoneIndex"].InnerText);
                // get textures
                string diffuse = meshNode["Textures"]["Diffuse"].InnerText;
                string normal = meshNode["Textures"]["Normal"].InnerText;
                string specular = meshNode["Textures"]["Specular"].InnerText;

                // create a mesh data
                Mesh m = new Mesh();
                m.Index = index;
                m.BoneIndex = boneIndex;
                m.Textures.Diffuse = diffuse;
                m.Textures.Normal = normal;
                m.Textures.Specular = specular;

                // add to the list of meshes
                meshes.Add(m);
            }

            // create a model description from reading xml file
            ModelXMLConfig modelConfig = new ModelXMLConfig(modelName, modelFilePath, modelScale, meshes);

            // #2 Build content loaded into description class by adding a new content found one-by-one
            string basePath = Path.GetDirectoryName(filePath);
            contentBuilder.Clear();

            // - add a model
            contentBuilder.Add(Path.Combine(basePath, modelConfig.FilePath), "Model", null, "ModelProcessor");
            // - add all textures from all meshes
            foreach(Mesh m in modelConfig.Meshes)
            {
                // diffuse
                contentBuilder.Add(Path.Combine(basePath, m.Textures.Diffuse), Path.GetFileNameWithoutExtension(m.Textures.Diffuse), "TextureImporter", "TextureProcessor");

                // normal
                if (!String.IsNullOrEmpty(m.Textures.Normal) && m.Textures.Normal != "null")
                {
                    contentBuilder.Add(Path.Combine(basePath, m.Textures.Normal), Path.GetFileNameWithoutExtension(m.Textures.Normal), "TextureImporter", "TextureProcessor");
                }

                // specular
                if (!String.IsNullOrEmpty(m.Textures.Specular) && m.Textures.Specular != "null")
                {
                    contentBuilder.Add(Path.Combine(basePath, m.Textures.Specular), Path.GetFileNameWithoutExtension(m.Textures.Specular), "TextureImporter", "TextureProcessor");
                }
            }

            // change status to loading
            this.Text += STATUS_LOADING_MODEL_FOR_RENDERING;

            // build the contents
            string buildError = contentBuilder.Build();
            if (string.IsNullOrEmpty(buildError))
            {
                // If the build succeeded, use the ContentManager to
                // load the temporary .xnb file that we just created.
                Model model = contentManager.Load<Model>("Model");
                ModelInfo modelInfo = MeasureModel(model);
                // reset camera position
                game.ResetCameraPosition(false);
                // set global scale (take scale from xml file as priority)
                game.GlobalModelScale = modelScale;
                // set the camera properties to xna game
                game.SetUpCamera(modelInfo.Center, modelInfo.Radius);

                // tag user-defined info to modelconfig
                UserTagModelLevel tag = new UserTagModelLevel();
                tag.Model = model;
                tag.BasePath = basePath;

                // tag user-defined infor in mesh level
                foreach (Mesh m in modelConfig.Meshes)
                {
                    UserTagMeshLevel tagMesh = new UserTagMeshLevel();
                    tagMesh.Diffuse = contentManager.Load<Texture2D>(Path.GetFileNameWithoutExtension(m.Textures.Diffuse));

                    if (!String.IsNullOrEmpty(m.Textures.Normal) && m.Textures.Normal != "null")
                    {
                        tagMesh.Normal = contentManager.Load<Texture2D>(Path.GetFileNameWithoutExtension(m.Textures.Normal));
                    }
                    if (!String.IsNullOrEmpty(m.Textures.Specular) && m.Textures.Specular != "null")
                    {
                        tagMesh.Specular = contentManager.Load<Texture2D>(Path.GetFileNameWithoutExtension(m.Textures.Specular));
                    }

                    // tag to each mesh
                    m.Tag = tagMesh;
                }

                modelConfig.Tag = tag;

                // tell xna game the model is loaded and set
                this.game.SetModelConfig(modelConfig);

                // change status to normal
                this.Text = baseFormTitle;
            }
            else
            {
                // If the build failed, display an error message.
                MessageBox.Show(buildError, "Error");

                // change status to normal
                this.Text = baseFormTitle;
            }

            Cursor = Cursors.Arrow;
        }

        public IntPtr getDrawSurface()
        {
            return outputImage.Handle;
        }
        
        /// <summary>
        /// Must be manually called after create xna game and before calling Run() of game.
        /// </summary>
        /// <param name="game"></param>
        /// <remarks>It's one time call.</remarks>
        public void SetUpXNAGame(Game1 game)
        {
            this.game = game;
            this.game.GlobalModelScale = GlobalSettings.GLOBAL_SCALE;

            ServiceContainer service = new ServiceContainer();
            service.AddService(typeof(IGraphicsDeviceService), game.Graphics);
            contentManager = new ContentManager(service, contentBuilder.OutputDirectory);
        }

        /// <summary>
        /// Whenever a new model is selected, we examine it to see how big
        /// it is and where it is centered. This lets us automatically zoom
        /// the display, so we can correctly handle models of any scale.
        /// </summary>
        ModelInfo MeasureModel(Model model)
        {
            // Look up the absolute bone transforms for this model.
            Matrix[] boneTransforms = new Matrix[model.Bones.Count];

            model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            // information (we will got their measurements after called this method)
            ModelInfo modelInfo;

            // Compute an (approximate) model center position by
            // averaging the center of each mesh bounding sphere.
            modelInfo.Center = Vector3.Zero;

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBounds = mesh.BoundingSphere;
                Matrix transform = boneTransforms[mesh.ParentBone.Index];
                Vector3 meshCenter = Vector3.Transform(meshBounds.Center, transform);

                modelInfo.Center += meshCenter;
            }

            modelInfo.Center /= model.Meshes.Count;

            // Now we know the center point, we can compute the model radius
            // by examining the radius of each mesh bounding sphere.
            modelInfo.Radius = 0;

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBounds = mesh.BoundingSphere;
                Matrix transform = boneTransforms[mesh.ParentBone.Index];
                Vector3 meshCenter = Vector3.Transform(meshBounds.Center, transform);

                float transformScale = transform.Forward.Length();

                float meshRadius = (meshCenter - modelInfo.Center).Length() +
                                   (meshBounds.Radius * transformScale);

                modelInfo.Radius = Math.Max(modelInfo.Radius, meshRadius);
            }

            return modelInfo;
        }

        private void modelScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // show model scale setting window
            ModelScaleSetWindow form = new ModelScaleSetWindow();
            form.ShowDialog(this);
        }

        private void ModelScaleSetWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            // we do nothing here //
            //if (e.KeyChar == 'f')
            //{
            //    // swap flag
            //    isWireframeDrawMode = !isWireframeDrawMode;
            //    this.game.SetWireFrameDrawMode(isWireframeDrawMode);

            //    e.Handled = true;
            //}
        }

        private void exportStructureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            // Default to the directory which contains our content files.
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            string relativePath = Path.Combine(assemblyLocation, "");
            string contentPath = Path.GetFullPath(relativePath);

            fileDialog.InitialDirectory = contentPath;

            // load .xml file
            fileDialog.Title = "Select a model file to export ...";

            fileDialog.Filter = "Model Files (*.fbx;*.x)|*.fbx;*.x|" +
                                "FBX Files (*.fbx)|*.fbx|" +
                                "X Files (*.x)|*.x|" +
                                "All Files (*.*)|*.*";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                exportModelStructure(fileDialog.FileName);
            }
        }

        private void exportModelStructure(string filePath)
        {
            // get the filename
            string fileName = Path.GetFileName(filePath);

            // Tell the ContentBuilder what to build.
            contentBuilder.Clear();
            contentBuilder.Add(filePath, "Model", null, "ModelProcessor");

            // change status title
            this.Text += STATUS_LOADING_MODEL_FOR_EXPORTING;
            // Build this new model data.
            string buildError = contentBuilder.Build();

            if (string.IsNullOrEmpty(buildError))
            {
                Model model = contentManager.Load<Model>("Model");

                // change status
                this.Text = baseFormTitle + STATUS_EXPORTING_MODEL_STRUCTURE;

                //write the structure into file
                modelWriter.Reload("Model", contentManager);
                modelWriter.WriteOut(Path.Combine(Path.GetDirectoryName(filePath), fileName) + ".out.txt");

                MessageBox.Show("Please check the export file in the same folder of the selected model.", "Export successfully");

                // change status back to normal
                this.Text = baseFormTitle;
            }
            else
            {
                MessageBox.Show(buildError, "Error");

                // change status back to normal
                this.Text = baseFormTitle;
            }
        }
    }
}
