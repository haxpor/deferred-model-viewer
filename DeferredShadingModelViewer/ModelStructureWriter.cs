﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DeferredShadingTest
{
    public class ModelStructureWriter
    {
        private string filename;
        private Model model;
        private Matrix[] boneTransforms;

        private BoundingBox bs;
        private BoundingSphere cover_bs;
        private BoundingSphere coverBB_bs;
        private float vertexCount;
        private float triangleCount;

        #region Properties
        /// <summary>
        /// Get the length along the x-dimension of the bounding box.
        /// </summary>
        /// <remarks>If the model is not yet processed then return -1.0f.</remarks>
        public float BoundingBoxLengthX
        {
            get
            {
                if (bs != null)
                    return bs.Max.X - bs.Min.X;
                else
                    return -1.0f;
            }
        }

        /// <summary>
        /// Get the length along the y-dimension of the bounding box.
        /// </summary>
        /// <remarks>If the model is not yet processed then return -1.0f.</remarks>
        public float BoundingBoxLengthY
        {
            get
            {
                if (bs != null)
                    return bs.Max.Y - bs.Min.Y;
                else
                    return -1.0f;
            }
        }

        /// <summary>
        /// Get the length along the z-dimension of the bounding box.
        /// <remarks>If the model is not yet processed then return -1.0f.</remarks>
        public float BoundingBoxLengthZ
        {
            get
            {
                if (bs != null)
                    return bs.Max.Z - bs.Min.Z;
                else
                    return -1.0f;
            }
        }

        /// <summary>
        /// Get the radius of the bounding sphere.
        /// <remarks>If the model is not yet processed then return -1.0f.</remarks>
        public float BoundingSphereRadius
        {
            get
            {
                if (cover_bs != null)
                    return cover_bs.Radius;
                else
                    return -1.0f;
            }
        }

        /// <summary>
        /// Get the radius of the bounding sphere over the bounding box.
        /// <remarks>If the model is not yet processed then return -1.0f.</remarks>
        public float BoundingSphereOverBoundingBoxRadius
        {
            get
            {
                if (coverBB_bs != null)
                    return coverBB_bs.Radius;
                else
                    return -1.0f;
            }
        }

        /// <summary>
        /// Gets the total count of vertex.
        /// </summary>
        public float VertexCount
        {
            get { return vertexCount; }
        }

        /// <summary>
        /// Gets the total count of triangle.
        /// </summary>
        public float TriangleCount
        {
            get { return triangleCount; }
        }

        #endregion Properties

        /// <summary>
        /// Create a model structure viewer.
        /// </summary>
        /// <param name="content">ContentManager</param>
        public ModelStructureWriter()
        {
        }

        /// <summary>
        /// Reload the model, and clear all the intermediate values used during the process.
        /// </summary>
        /// <param name="filename">Model filename</param>
        /// <param name="content">ContentManager object</param>
        public ModelStructureWriter(string filename, ContentManager content)
        {
            Reload(filename, content);
        }

        /// <summary>
        /// Reload the model, and clear all the intermediate values used during the process.
        /// </summary>
        /// <param name="filename">Model filename</param>
        public void Reload(string filename, ContentManager content)
        {
            this.filename = filename;
            model = content.Load<Model>(this.filename);
            // copy bone transforms
            boneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            // init those bounding object to zero (dummy)
            bs = new BoundingBox(Vector3.Zero, Vector3.Zero);
            cover_bs = new BoundingSphere(Vector3.Zero, 0.0f);
            coverBB_bs = new BoundingSphere(Vector3.Zero, 0.0f);
            // clear the number of polygon to 0
            vertexCount = 0;
            triangleCount = 0;
        }

        /// <summary>
        /// Get the bounding box covers all meshes of the model being processed.
        /// </summary>
        /// <returns>Bounding Box covers all meshes of the model.</returns>
        public BoundingBox GetModelBoundingBox()
        {
            return bs;
        }

        /// <summary>
        /// Get the bounding sphere covers all meshes of the model being processed.
        /// </summary>
        /// <returns>Bounding sphere covers all meshes of the model being processed.</returns>
        public BoundingSphere GetModelBoundingSphere()
        {
            return cover_bs;
        }

        /// <summary>
        /// Get the bounding sphere covers bounding box of the model being processed.
        /// </summary>
        /// <returns>Bounding sphere covers bounding box of the model being processed.</returns>
        public BoundingSphere GetModelBoundingSphereOverBoundingBox()
        {
            return coverBB_bs;
        }

        public void WriteOut(string outputFile)
        {
            StreamWriter writer = new StreamWriter(outputFile);

            writer.WriteLine("-- Model Bone Information --");
            ModelBone rootBone = model.Root;
            WriteBones(rootBone, 0, writer);

            writer.WriteLine();

            writer.WriteLine("-- Model Mesh Information --");
            int id = 0;

            // the minimum, and maximum position define the bounding box
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            // our vertex data
            float[] vertices;

            // *we do the necessary calculations here as to avoid the overhead (pack in only 1 loop)
            foreach (ModelMesh mesh in model.Meshes)
            {
                // # Do the bounding box
                // get the vertex data (for each modelmesh)
                vertices = new float[mesh.VertexBuffer.SizeInBytes / sizeof(float)];
                mesh.VertexBuffer.GetData<float>(vertices);

                // Loop through all the model meshparts
                // Note:
                // traverse though all the vertex
                // only grab the first portion of the vertex data, the less we will ignore in this case.
                for (int k = 0; k < mesh.MeshParts.Count; k++)
                {
                    for (int i = 0; i < mesh.VertexBuffer.SizeInBytes / sizeof(float); i += mesh.MeshParts[k].VertexStride / sizeof(float))
                    {
                        Vector3 pos = new Vector3(vertices[i], vertices[i + 1], vertices[i + 2]);

                        // transform this vertex
                        pos = Vector3.Transform(pos, boneTransforms[mesh.ParentBone.Index]);

                        min = Vector3.Min(min, pos);
                        max = Vector3.Max(max, pos);

                        //# Calculate the count of vertex
                        vertexCount += 1.0f;
                    }

                    //# calculate total count of triangle
                    triangleCount += mesh.MeshParts[k].PrimitiveCount;
                }

                // # Do the bounding sphere
                cover_bs = BoundingSphere.CreateMerged(cover_bs, mesh.BoundingSphere);

                //VertexBuffer vb= mesh.VertexBuffer.B
                WriteMeshes(mesh, id++, writer);
            }

            // create the bounding box
            bs = new BoundingBox(min, max);
            // create our bounding sphere from bounding box
            coverBB_bs = BoundingSphere.CreateFromBoundingBox(bs);

            writer.WriteLine();
            writer.WriteLine("-- Model Bounding Box --");
            WriteBoundingBox(bs, writer);

            writer.WriteLine();
            writer.WriteLine("-- Model Bounding Sphere (Cover all meshes of model)--");
            WriteBoundingSphere(cover_bs, writer, BoundingSphereRadius);

            writer.WriteLine();
            writer.WriteLine("-- Model Bounding Sphere (Cover the bounding box)--");
            WriteBoundingSphere(coverBB_bs, writer, BoundingSphereOverBoundingBoxRadius);

            writer.WriteLine();
            writer.WriteLine("-- Model Misc Statistics --");
            WriteMiscStats(writer);

            writer.Close();
        }

        private void WriteBones(ModelBone bone, int level, StreamWriter writer)
        {
            for (int i = 0; i < level; i++)
                writer.Write("\t");
            //Name
            if (bone.Name == "" || bone.Name == null)
                writer.WriteLine("null");
            else
                writer.WriteLine("-Name: " + bone.Name);
            //Index
            for (int i = 0; i < level; i++)
                writer.Write("\t");
            writer.WriteLine(" Index: " + bone.Index);

            //traverse through its children
            foreach (ModelBone childBone in bone.Children)
            {
                WriteBones(childBone, level + 1, writer);
            }
        }

        private void WriteMeshes(ModelMesh mesh, int id, StreamWriter writer)
        {
            writer.WriteLine("ID: " + id);
            writer.WriteLine("Name: " + mesh.Name);
            writer.WriteLine("PBone: " + mesh.ParentBone.Name + " (" + mesh.ParentBone.Index + ")");
            writer.WriteLine();
        }

        /// <summary>
        /// Write the length diemension of the model's bounding box.
        /// </summary>
        /// <param name="bb">Bounding box covers all meshes of the model</param>
        /// <param name="writer">StreamWriter object</param>
        private void WriteBoundingBox(BoundingBox bb, StreamWriter writer)
        {
            writer.WriteLine("X: " + BoundingBoxLengthX);
            writer.WriteLine("Y: " + BoundingBoxLengthY);
            writer.WriteLine("Z: " + BoundingBoxLengthZ);
            System.Console.Write(bb.Max.Z + ", " + bb.Min.Z);
        }

        /// <summary>
        /// Write the length dimension of the model's bounding sphere.
        /// </summary>
        /// <param name="bs">Bounding sphere</param>
        /// <param name="writer">StreamWriter object</param>
        /// <param name="value">Value of radius to write</param>
        /// <remarks>We ignore the position of the model as we just initially load the model before updating.</remarks>
        private void WriteBoundingSphere(BoundingSphere bs, StreamWriter writer, float value)
        {
            writer.WriteLine("Radius: " + value);
        }

        /// <summary>
        /// Write the misc statistics of the model.
        /// </summary>
        /// <param name="writer">StreamWriter object</param>
        private void WriteMiscStats(StreamWriter writer)
        {
            writer.WriteLine("Vertices: " + vertexCount);
            writer.WriteLine("Triangles: " + triangleCount);
        }
    }
}
