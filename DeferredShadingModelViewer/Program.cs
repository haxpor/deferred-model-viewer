﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DeferredShadingTest;

namespace DeferredShadingModelViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                // create form
                Form1 form1 = new Form1();
                form1.Show();
                // create xna game
                Game1 game = new Game1(form1.getDrawSurface());
                form1.SetUpXNAGame(game);
                game.Run();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(form1);
        }
    }
}
