#region File Description
//-----------------------------------------------------------------------------
// ServiceContainer.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
#endregion

namespace DeferredShadingModelViewer
{
    public class ServiceContainer : IServiceProvider
    {
        // Contains the service types and services
        Dictionary<Type, object> services = new Dictionary<Type, object>();

        // Add a new service
        public void AddService(Type Service, object Provider)
        {
            // If we already have this type of service provider, throw an
            // exception
            if (services.ContainsKey(Service))
                throw new System.Exception("The service container already has a "
            + "service provider of type " + Service.Name);

            // Otherwise, add it to the list
            this.services.Add(Service, Provider);
        }

        // Get a service from the service container
        public object GetService(Type Service)
        {
            // If we have this type of service, return it
            foreach (Type type in services.Keys)
                if (type == Service)
                    return services[type];

            // Otherwise, throw an exception
            throw new System.Exception("The service container does not contain "
            + "a service provider of type " + Service.Name);
        }

        // A shortcut way to get a service. The benefit here is that we
        // can specify the type in the brackets and also return the
        // service of that type. For example, instead of
        // "Camera cam = (Camera)Services.GetService(typeof(Camera));",
        // we can use "Camera cam = Services.GetService()"
        public T GetService<T>()
        {
            object result = GetService(typeof(T));

            if (result != null)
                return (T)result;

            return default(T);
        }

        // Removes a service provider from the container
        public void RemoveService(Type Service)
        {
            if (services.ContainsKey(Service))
                services.Remove(Service);
        }

        // Gets whether or not the container has a provider of this type
        public bool ContainsService(Type Service)
        {
            return services.ContainsKey(Service);
        }
    }
}
