﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DeferredShadingModelViewer
{
    public partial class ModelScaleSetWindow : Form
    {
        public ModelScaleSetWindow()
        {
            InitializeComponent();
        }

        private void ModelScaleSetWindow_Load(object sender, EventArgs e)
        {
            // set the global scale value in the textbox
            Form1 form = (Form1)Owner;
            modelScaleInput.Text = form.GlobalModelScale.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // set the global scale back, and close this form
            Form1 form = (Form1)Owner;
            form.GlobalModelScale = Convert.ToSingle(modelScaleInput.Text);

            Close();
        }
    }
}
