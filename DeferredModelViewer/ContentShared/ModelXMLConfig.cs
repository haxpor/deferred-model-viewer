using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace ContentShared
{
    /// <summary>
    /// Textures file path appearing in xml config file.
    /// </summary>
    public struct Textures
    {
        public string Diffuse;
        public string Normal;
        public string Specular;
    }

    /// <summary>
    /// Mesh structure appearing in xml config file.
    /// </summary>
    public class Mesh
    {
        public int Index;
        public int BoneIndex;
        public Textures Textures;
        public Object Tag;
    }

    /// <summary>
    /// User-defined information in model level.
    /// </summary>
    public class UserTagModelLevel
    {
        public string BasePath;     // base path
        public Model Model;         // loaded model
    }

    /// <summary>
    /// User-defined information in mesh level.
    /// </summary>
    public class UserTagMeshLevel
    {
        public Texture2D Diffuse;
        public Texture2D Normal;
        public Texture2D Specular;
    }

    /// <summary>
    /// XML config file for model being read in from the main custom .xml file input into an application.
    /// </summary>
    /// <remarks>Please refer to the ship1.xml file for example.</remarks>
    public class ModelXMLConfig
    {
        public string Name;
        public string FilePath;
        public float Scale;
        public List<Mesh> Meshes;
        public Object Tag;   // tag loaded model

        public ModelXMLConfig()
        {
            Name = null;
            FilePath = null;
            Scale = 1.0f;
            Tag = null;

            // create an empty list of meshes
            Meshes = new List<Mesh>();
        }

        public ModelXMLConfig(string name, string filePath, float scale, List<Mesh> meshes)
        {
            Name = name;
            FilePath = filePath;
            Scale = scale;
            Meshes = meshes;
            Tag = null;
        }
    }
}
